# -*- coding: utf-8 -*-
"""
Created on Wed Mar 26 17:05:13 2014

@author: doc
"""

# MODEL PREFIX, TYPES & MAPPINGS
MODEL_PREFIX = "icd10_pcs."
# TYPES
MODEL_PCSTABLE  = "PcsTable"
MODEL_PCSROW    = "PcsRow"

AXIS_SECTION    = "Section"
AXIS_BODYSYSTEM = "BodySystem"
AXIS_OPERATION  = "Operation"
AXIS_BODYPART   = "BodyPart"
AXIS_APPROACH   = "Approach"
AXIS_DEVICE     = "Device"
AXIS_QUALIFIER  = "Qualifier"

AXIS_TITLE = (
    0,
    AXIS_SECTION,
    AXIS_BODYSYSTEM,
    AXIS_OPERATION,
    AXIS_BODYPART,
    AXIS_APPROACH,
    AXIS_DEVICE,
    AXIS_QUALIFIER
)
# MAPPINGS
# NB: some axis title inconsistencies were found, hence multiple entries...
#MODELS = {
#           "PcsTable"               : MODEL_PCSTABLE, 
#           "PcsRow"                 : MODEL_PCSROW, 
#           "Body Part"              : MODEL_BODYPART, 
#           "Body System"            : MODEL_BODYPART, 
#           "Body Region"            : MODEL_BODYPART, 
#           "Body System / Region"   : MODEL_BODYPART, 
#           "Approach"               : MODEL_APPROACH, 
#           "Device"                 : MODEL_DEVICE, 
#           "Substance"              : MODEL_DEVICE, 
#           "Qualifier"              : MODEL_QUALIFIER
#         }
#AXIS_TYPE = (
#    MODEL_PCSTABLE, 
#    MODEL_PCSROW, 
#    MODEL_BODYPART, 
#    MODEL_BODYPART, 
#    MODEL_BODYPART, 
#    MODEL_BODYPART, 
#    MODEL_APPROACH, 
#    MODEL_DEVICE, 
#    MODEL_DEVICE, 
#    MODEL_QUALIFIER
#)

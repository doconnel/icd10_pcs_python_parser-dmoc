# -*- coding: utf-8 -*-
"""
Created on Sun Apr  6 07:55:24 2014

@author: doc

This is scratchpad type code so little in the way of comments or explanations. 
It is being used to explore the XML data prior to creating a schema and 
program to convert XML to sqlite db (separate notebook/py file).


"""

import xml.etree.cElementTree as ET

tree = ET.parse("icd10pcs_tabular_2014.xml")
root = tree.getroot()


#======================================================================
# UTILITIES

def unique(tag, target=None):
    """Returns unique set, len, total"""
    uniq = target if target else set()
    i = 0
    for i,e in enumerate(root.iterfind(tag)):
        uniq.add(e.text) # No test!
    return uniq, len(uniq), i
    
def iter_len(tag):
    """Memory efficient len() for iterator"""
    return sum(1 for _ in root.iterfind(tag))


#======================================================================
# QUICK CHECKS & TOTALS

# Total Tables and Table-Axes
ntables = iter_len(".//pcsTable")
ntable_axes = iter_len(".//pcsTable/axis")
print "We have %s tables so should have %s x 3 = %s table axes: %s" % (ntables, ntables, 3*ntables, ntable_axes)

# Total Row and Row-Axes
nrows = iter_len(".//pcsRow")
nrow_axes = iter_len(".//pcsRow/axis")
print "We have %s rows so should have %s x 4 = %s row axes: %s" % (nrows, nrows, 4*nrows, nrow_axes)

# Total Axes
naxes = iter_len(".//axis")
print "So now we should have %s + %s = %s axes: %s" % (ntable_axes, nrow_axes, ntable_axes+nrow_axes, naxes)

# Total Titles: db title + naxes
print "Total titles (naxes + 1 for db) : %s" % iter_len(".//title")

# Total Labels (cannot pre-calc because # varies per row-axis)
print "Total labels : %s" % iter_len(".//label")


#======================================================================
# REDUNDANCY FOR ALL TITLES & LABELS PER AXIS

def axis_stats():
    """Determine redundancy of titles and labels for each axis type (1..7)"""
    
    print "(axis type, unique #, total #, redundancy %)"
    
    unique_total = 0
    for tag in ("title", "label"):
        print "\n%s:\n" % tag
        for n in range(1,8):
            u,i,t = unique(".//axis[@pos='%s']/%s" % (n, tag))
            print n, i, t, int(100 * (1-float(i)/t))
            unique_total += i
    
    print "\nunique_total: %s" % unique_total
            
axis_stats()


#======================================================================
# REDUNDANCY FOR ALL TITLES & LABELS COMBINED

all_unique_text, i, t = unique(".//title")
all_unique_text, i, t = unique(".//label", target=all_unique_text)
print len(all_unique_text)

# ...less than previous total so small amount of text duplicated between 
# different axes.


#======================================================================
# PREP FOR REMOVING REDUNDANCY

# "all_unique_text" can now be used to remove redundancy by replacing xml text 
# with index into sorted version...

text_table = list(all_unique_text)
text_table.sort()

# Print a sample of the table and a look-up...
print "\n".join(text_table[:10])
print
print text_table.index("2nd Toe, Left") # ZERO-BASED INDEX!


#======================================================================
# MORE REDUNDANCY FOUND IN "DEFINITIONS" (axis/pos=3)
# ("definitions" are not mandatory!)

def xdefinition(e):
    global c1,c2
    c1 += 1
    try:
        return e.find('definition').text
    except:
        c2 += 1
        #print c1, e, e.attrib['pos']
        return None

# Get text in all "definition" tags for "Operation" axes...

# c1: count total "Operation" (@pos==3) axes
# c2: count the ones with no definition tag

c1 = c2 = 0
xaxes = root.iterfind(".//axis[@pos='3']")
defs = [xdefinition(a) for a in xaxes if a]
print "%s Operation axes do not have definition tags" % c2

# Get unique text
uset = set()
for d in defs:
    uset.add(d)
    
# Redundancy in definitions...

t = len(defs)
u = len(uset)
print "Redundancy (%s from %s): %s" % (u, t, int(100 * (1-float(u)/t)))
# ... ~87% redundant.
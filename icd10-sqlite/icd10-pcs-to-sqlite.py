# -*- coding: utf-8 -*-
"""
Created on Sun Apr  6 07:55:24 2014

@author: doc


            >>>>> Copy or link to XML before running! <<<<<


ICD-10-PCS XML-TO-SQLITE DATABASE

This version mirrors the XML structure which means little/no assumptions are 
being made about how the data may be used. This may change but is a good 
starting point and helps tease out any perculiarities in the data. 

In the process of normalising it becomes clear that since XML does not need to 
have a fixed rigid structure that "Axis" is playing several roles. This further 
leads to making a distinction between table-axis labels and row-axis labels. 

The current version produces a db of 0.739M compared to 2.5M of XML, a 
reduction of 70%. For DrugBank this may translate into a saving of 165M!

See DB_SCHEMA constant below for lates DB design
"""

import xml.etree.cElementTree as ET
import sqlite3
from datetime import datetime


#======================================================================
# CONSTANTS

DB_SCHEMA = [
"""
CREATE TABLE IF NOT EXISTS PcsText(
    id integer,
	pcs_text text,
    PRIMARY KEY(id)
);
""",
"""
CREATE TABLE IF NOT EXISTS PcsTable(
    id integer,
    PRIMARY KEY(id)
);
""",
"""
CREATE TABLE IF NOT EXISTS PcsRow(
    id integer,
	table_id_fk integer,
    PRIMARY KEY(id),
    FOREIGN KEY(table_id_fk) REFERENCES PcsTable(id)
);
""",
"""
CREATE TABLE IF NOT EXISTS PcsTableAxis(
    id integer,
	table_id_fk integer,
	pos integer,
	nvalues integer,
	title_text_id_fk integer,
	definition_text_id_fk text,
    PRIMARY KEY(id),
    FOREIGN KEY(table_id_fk) REFERENCES PcsTable(id),
    FOREIGN KEY(title_text_id_fk) REFERENCES PcsText(id)
    FOREIGN KEY(definition_text_id_fk) REFERENCES PcsText(id)
);
""",
"""
CREATE TABLE IF NOT EXISTS PcsRowAxis(
    id integer,
	row_id_fk integer,
	pos integer,
	nvalues integer,
	title_text_id_fk integer,
    PRIMARY KEY(id),
    FOREIGN KEY(row_id_fk) REFERENCES PcsRow(id),
    FOREIGN KEY(title_text_id_fk) REFERENCES PcsText(id)
);
""",
"""
CREATE TABLE IF NOT EXISTS PcsTableAxisLabel(
    id integer,
	axis_id_fk integer,
	code text,
	label_text_id_fk integer,
    PRIMARY KEY(id),
    FOREIGN KEY(axis_id_fk) REFERENCES PcsTableAxis(id)
    FOREIGN KEY(label_text_id_fk) REFERENCES PcsText(id)
);
""",
"""
CREATE TABLE IF NOT EXISTS PcsRowAxisLabel(
    id integer,
	axis_id_fk integer,
	code text,
	label_text_id_fk integer,
    PRIMARY KEY(id),
    FOREIGN KEY(axis_id_fk) REFERENCES PcsRowAxis(id)
    FOREIGN KEY(label_text_id_fk) REFERENCES PcsText(id)
);
"""
]

SQL_INSERT = "INSERT INTO %s VALUES%s;"


#======================================================================
# GLOBALS

root = None
db_name = db = None
pcs_table_id = pcs_row_id = 0
pcs_table_axis_id = pcs_row_axis_id = 0
pcs_table_axis_label_id = pcs_row_axis_label_id = 0
unique_text = None


#======================================================================
# UTILITIES

def time_stamp(fmt='%Y%m%d-%H%M%S'):
    return datetime.now().strftime(fmt)

def id_of_text(text):
    """One-based ID (instead of zero-based index)"""
    return unique_text.index(text)+1


#======================================================================
# XML helpers, in a non-functional style ;-)

def xtables(root):
    return root.iter('pcsTable')

def xrows(table):
    return table.iter('pcsRow')

def xaxes(e, max_num):
    return e.findall('axis')[:max_num]

def xtitle(e):
    return e.text if e.tag == 'title' else e.find('title').text

def label(e):
    return e.text if e.tag == 'label' else e.find('label').text

def xdefinition(e):
    try:
        return e.find('definition').text
    except:
        return ''

def xattrib(e, attrib):
    return e.attrib[attrib]

def xtable_axes(table):
    return xaxes(table, 3)

def xrow_axes(row):
    return xaxes(row, 4)

#def xtable_labels(table):
#    return (xlabel(a) for a in xtable_axes(table))

def xaxis_labels(axis):
    return axis.findall("label")


#======================================================================
# MAIN FUNCTIONS

def zero_ids():
    global pcs_table_id, pcs_row_id
    global pcs_table_axis_id, pcs_row_axis_id
    global pcs_table_axis_label_id, pcs_row_axis_label_id
    
    pcs_table_id = pcs_row_id = 0
    pcs_table_axis_id = pcs_row_axis_id = 0
    pcs_table_axis_label_id = pcs_row_axis_label_id = 0
    
def get_unique(xpath, target=None):
    """Returns unique set"""
    uniq = target if target!=None else set()
    for e in root.iterfind(xpath):
        try:
            uniq.add(e.text)
        except:
            pass
    return uniq
    
def db_table_insert(table, fields):
    f = str(fields) if type(fields)==tuple else "({})".format(fields)
    db.cursor().execute(SQL_INSERT % (table, f))

def populate_pcs_text():
    """Title & Label text combined to remove redundancy"""
    
    global unique_text
    
    unique_text = get_unique(".//title")
    get_unique(".//label", target=unique_text)
    get_unique(".//axis[@pos='3']/definition", target=unique_text)
    
    unique_text = list(unique_text)
    unique_text.sort()
    unique_text = [''] + unique_text
    
    for i,t in enumerate(unique_text):
        db_table_insert("PcsText", (i+1, t))
    db.commit()

def process_pcs_table(e):
    global pcs_table_id
    
    pcs_table_id += 1
    
    db_table_insert("PcsTable", pcs_table_id)
    #return fields
    return xtable_axes(e), xrows(e)

def process_pcs_row(e):
    global pcs_row_id
    
    pcs_row_id += 1
    
    fields = (pcs_row_id, pcs_table_id)
    db_table_insert("PcsRow", fields)
    #return fields
    return xrow_axes(e)

def process_pcs_table_axis(e):
    global pcs_table_axis_id
    
    pcs_table_axis_id += 1
    
    pos = int(xattrib(e, "pos"))
    nvalues = int(xattrib(e, "values"))
    title_text_id_fk = id_of_text(xtitle(e))
    
    definition_text_id_fk = 1 # Default
    if pos == 3:
        definition_text_id_fk = id_of_text(xdefinition(e))
    
    fields = (pcs_table_axis_id, pcs_table_id, 
              pos, nvalues, title_text_id_fk, definition_text_id_fk)
    db_table_insert("PcsTableAxis", fields)
    #return fields
    return xaxis_labels(e)
    
def process_pcs_row_axis(e):
    global pcs_row_axis_id
    
    pcs_row_axis_id += 1
    
    pos = int(xattrib(e, "pos"))
    nvalues = int(xattrib(e, "values"))
    title_text_id_fk = id_of_text(xtitle(e))
    
    fields = (pcs_row_axis_id, pcs_row_id, pos, nvalues, title_text_id_fk)
    db_table_insert("PcsRowAxis", fields)
    #return fields
    return xaxis_labels(e)
    
def process_pcs_table_axis_label(e):
    global pcs_table_axis_label_id
    
    pcs_table_axis_label_id += 1
    
    code = xattrib(e, "code")
    label_text_id_fk = id_of_text(label(e))
    
    fields = (pcs_table_axis_label_id, pcs_table_axis_id, 
              code, label_text_id_fk)
    db_table_insert("PcsTableAxisLabel", fields)
    #return fields
    
def process_pcs_row_axis_label(e):
    global pcs_row_axis_label_id
    
    pcs_row_axis_label_id += 1
    
    code = xattrib(e, "code")
    label_text_id_fk = id_of_text(label(e))
    
    fields = (pcs_row_axis_label_id, pcs_row_axis_id, code, label_text_id_fk)
    db_table_insert("PcsRowAxisLabel", fields)
    #return fields
    
def create_db():
    global db_name, db
    
    db_name = "test" + time_stamp() + ".db"
    db = sqlite3.connect(db_name)
    
    db.execute("PRAGMA foreign_keys = ON")
    
    for t in DB_SCHEMA:
        db.cursor().execute(t)
        db.commit()
    
def populate_db():
    
    zero_ids()
    
    populate_pcs_text()
    
    # Mirrors XML struture (see Jackson Structure Programming)...
    for t in xtables(root):
        axes, rows = process_pcs_table(t)
        
        for a in axes:
            labels = process_pcs_table_axis(a)
            for l in labels:
                process_pcs_table_axis_label(l)
                
        for r in rows:
            axes = process_pcs_row(r)
            
            for a in axes:
                labels = process_pcs_row_axis(a)
                for l in labels:
                    process_pcs_row_axis_label(l)
                
    db.commit()
    

#======================================================================
# EXPORTERS

# SELECT max(id) FROM PcsTable

def export_text():
    pass

def export_json():
    pass

def export_yaml():
    pass

def export_all():
    pass


#======================================================================
def main():
    global root
    
    tree = ET.parse("icd10pcs_tabular_2014.xml")
    root = tree.getroot()
    
    create_db()
    populate_db()
    print "Created: " + db_name


#======================================================================
if __name__ == "__main__":
    # >>>>> Copy or link to XML before running! <<<<<
    main()


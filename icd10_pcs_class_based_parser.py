#! /usr/bin/python

# LICENSE: GNU-GPL Version 3
# Author: Dr. Easwar T.R
# Date: 26-03-2014
# Code is part of icd10_pcs_python_parser repo at http://bitbucket.org/dreaswar/icd10_pcs_python_parser


#Import The Requirements
import xml.etree.ElementTree as ET
import sqlite3
import yaml
import json


class Title(object):
    def __init__(self, title):
      self.title = title
      self.text = getattr( self.title,'text', None)

class Label(object):
    def __init__(self, label):
      self.label = label
      self.text = getattr( self.label,'text', None)
      self.code = getattr( self.label, 'get', None)('code')
 

class Axis(object):
    def __init__(self, axis):

       self.axis = axis

       self.values = getattr(self.axis,'get',None)('values')
       self.position = getattr(self.axis,'get',None)('position')

       self.titles = []
       self.labels = []
       for label in self.axis.findall('label'):
           self.labels.append( Label(label) )

       if self.axis.findall('title') is not None:
          for title in self.axis.findall('title'):
            self.titles.append( Title(title) )
       else:
          self.titles = None
 

class PcsRow(object):
    
    def __init__(self, row):

       self.row = row
       self.axis = []
       for axis in self.row.findall('axis'):
          self.axis.append( Axis(axis) )
      

class PcsTable(object):

    def __init__(self, table):
        self.table = table
        self.axis = []
        self.pcs_rows = []

        for axis in  table.findall('axis'):
           self.axis.append( Axis(axis) )

        for row in self.table.findall('pcsRow'):
           self.pcs_rows.append( PcsRow(row) )


class RootXML(object):
    
    def __init__(self, path=None):

        if not path:
          self.path = 'data/icd10pcs_tabular_2014.xml'
        else:
          self.path = path

        try:
          self.tree = ET.parse(self.path)
        except (IOError):
          raise IOError

        self.root = self.tree.getroot()
        self.pcs_tables = []

        self._build_pcs_tables()

        
    def _build_pcs_tables(self):
        all_pcs_tables = self.root.iter('pcsTable')
        for pcs_table in all_pcs_tables:
           self.pcs_tables.append( PcsTable(pcs_table) )

       

if __name__ == '__main__':
    
    # Initializing the RootXML class initializes other classes and collects data as instances and attributes of instances
    # With Chained initialisation of the PcsTables, PcsRow, Axis etc... the pcs_tables attribute can hold all the data for the XML in object form. 
    # Can this be used to construct / serialize into JSON / YAML / SQL without knowing what tag names are ? 
    # All data gets accumulated inside this instance attribute
    # Now question is to construct a serializer that retrieves and presents data the way we want into JSON / YAML
    # Of course performance is another question....
    rootXML = RootXML() 
    
   # This will print the label PCS Code
   print ( rootXML.pcs_tables[0].pcs_rows[0].axis[0].labels[0].code)
   # This will print label PCS Text
   print ( rootXML.pcs_tables[0].pcs_rows[0].axis[0].labels[0].text)
    
    with file('data/icd10pcs_tabular_2014-cb.json','w') as f:
        f.write(json.dumps(rootXML, indent=4))
    with file('data/icd10pcs_tabular_2014-cb.yaml','w') as f:
        yaml.dump_all(rootXML, f)#, indent=4) #, default_flow_style=False)

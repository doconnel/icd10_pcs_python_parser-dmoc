# -*- coding = utf-8 -*-
"""
Created on Tue Mar 18 09:57:30 2014

@author = doc
"""

import os
#from __future__ import print_function
import sqlite3


DEBUG = False
DEBUG_PREFIX = "SQL: "

DEBUG_ONLY = False

DATADIR  = 'data'
FILENAME = 'icd10pcs'
FILEEXT  = '.db'
FILEPATH = os.path.join(DATADIR, FILENAME+FILEEXT)


translation = None
level = 0
db = None

__create_table_template = """CREATE TABLE IF NOT EXISTS {}({});\n"""

def __sql_create_table(table_name, fields):
    return __create_table_template.format(table_name, fields)

__sql_create_table_pcsTable = __sql_create_table("pcsTable", """
    sec_id integer,
    section text,
    body_system text,
    operation text,
    PRIMARY KEY(sec_id)
""")

__sql_create_table_pcsRow = __sql_create_table("pcsRow", """
    pcsRow_id integer,
    pcsTable_fk integer,
    FOREIGN KEY(pcsTable_fk) REFERENCES pcsTable(sec_id),
    PRIMARY KEY(pcsRow_id)
""")

__sql_create_table_bodyPart = __sql_create_table("bodyPart", """
    body_part text,
    pcsRow_fk integer,
    FOREIGN KEY(pcsRow_fk) REFERENCES pcsRow(pcsRow_id)
""")

__sql_create_table_approach = __sql_create_table("approach", """
    approach text,
    pcsRow_fk integer,
    FOREIGN KEY(pcsRow_fk) REFERENCES pcsRow(pcsRow_id)
""")

__sql_create_table_device = __sql_create_table("device", """
    device text,
    pcsRow_fk integer,
    FOREIGN KEY(pcsRow_fk) REFERENCES pcsRow(pcsRow_id)
""")

__sql_create_table_qualifier = __sql_create_table("qualifier", """
    qualifier text,
    pcsRow_fk integer,
    FOREIGN KEY(pcsRow_fk) REFERENCES pcsRow(pcsRow_id)
""")

#print __sql_create_table_pcsTable

__sql_tables = [__sql_create_table_pcsTable,
                __sql_create_table_pcsRow,
                __sql_create_table_bodyPart,
                __sql_create_table_approach,
                __sql_create_table_device,
                __sql_create_table_qualifier
]
#print __sql_tables

SQL_PCSTABLE_INSERT = "INSERT INTO pcsTable VALUES(%d, '%s','%s','%s');"
SQL_PCSROW_INSERT   = "INSERT INTO pcsRow VALUES(%d,%d);"
SQL_PCSLABEL_INSERT = "INSERT INTO %s VALUES('%s',%d);"


def debug(msg, force=False):
    """Module debugging"""
    if DEBUG or force:
        print "{}: {}".format(__name__,msg)
        

def db_execute(sql):
    """SQL Execute"""
    if not DEBUG_ONLY:
        return db.cursor().execute(sql)
    return True
    
def db_commit_now(sql):
    """SQL Execute and commit"""
    if not DEBUG_ONLY:
        db_execute(sql)
        return db.commit()
    return True
    
def db_insert_pcs_table(fields):
    """Insert PCS Table"""
    db_commit_now(SQL_PCSTABLE_INSERT % fields)
    
def db_insert_pcs_row(fields):
    """Insert PCS Row"""
    db_commit_now(SQL_PCSROW_INSERT % fields)

def db_insert_pcs_label(table_and_fields):
    """Insert PCS Label (see GRP_MODELS or LABEL_TABLES)"""
    db_commit_now(SQL_PCSLABEL_INSERT % table_and_fields)

def db_create_tables():
    """Create tables"""
    for sql in __sql_tables:
        try:
            #print "TABLE: "+sql
            db_commit_now(sql)
            #debug(sql)
        except AttributeError:
            debug(sql, True)

def db_init(filepath):
    """Create the database and tables"""
    global db
    
    debug("Setting up DB connection and Cursor")
    db = sqlite3.connect(FILEPATH)
    db.execute("PRAGMA foreign_keys = ON")
    db_create_tables()


def translate_pcs_table(pcs_table_id, section, body_system, operation):
    """Translate PCS Table"""
    db_insert_pcs_table((pcs_table_id, section, body_system, operation))
    debug("Committed pcsTable: {}".format(pcs_table_id))
    return True
    
def translate_pcs_row(pcs_row_id, pcs_table_fk):
    """Translate PCS Row"""
    db_insert_pcs_row((pcs_row_id, pcs_table_fk))
    debug("Committed pcsRow: {}".format(pcs_row_id))
    return True
    
def translate_pcs_label(table, label_id, pcs_row_fk, label_text):
    """Translate PCS Data (see GRP_MODELS or DATA_TABLES)"""
    debug("Committing: {}/{}/{}".format(table, label_text, pcs_row_fk))
    db_insert_pcs_label((table, label_text, pcs_row_fk))
    debug("Committed: {}/{}/{}".format(table, label_text, pcs_row_fk))
    return True
    
def translate_set_level(new_level):
    """Set level"""
    global level
    level = new_level


def init(datadir, filestub):
    """Translator initialization"""
    global DATADIR, FILEPATH
    DATADIR = datadir
    FILEPATH = os.path.join(DATADIR, filestub+FILEEXT)
    db_init(FILEPATH)

def clean_up():
    """Translator clean up"""
    pass
  

if __name__ == '__main__':
    for t in __sql_tables:
        print t
# -*- coding = utf-8 -*-
"""
Created on Tue Mar 18 09:57:30 2014

@author = doc
"""

import os
#from __future__ import print_function
import json, yaml


DEBUG = False
DEBUG_PREFIX = "JSON: "

DATADIR  = "data"
FILESTUB = "icd10pcs"
FILEEXT_JSON  = ".json"
FILEEXT_YAML  = ".yaml"
#FILEPATH = os.path.join(DATADIR, FILESTUB+FILEEXT)


level = 0


def debug(msg):
    """Module debugging"""
    if DEBUG:
        print "{}: {}".format(__name__,msg)


#==========================================================

def init(datadir, filestub):#, streaming):
    """Translator initialization"""
    global DATADIR, FILESTUB
    DATADIR, FILESTUB = datadir, filestub
    #FILEPATH = os.path.join(DATADIR, filestub+FILEEXT)

def set_level(new_level):
    """Set level"""
    global level
    if new_level > level:
        pass
    elif new_level < level:
        pass
    level = new_level

def new_chunk(level, chunk_type, chunk):
    pass

def save(translation):
    """Save translation to file"""
    filepath = os.path.join(DATADIR, FILESTUB+FILEEXT_JSON)
    with file(filepath,'w') as f:
        f.write(json.dumps(translation, indent=4))
    filepath = os.path.join(DATADIR, FILESTUB+FILEEXT_YAML)
    with file(filepath,'w') as f:
        yaml.dump_all(translation, f)#, indent=4) #, default_flow_style=False)

def clean_up():
    """Translator clean up"""
    pass


#==========================================================
if __name__ == '__main__':
    pass